using System;
using Microsoft.AspNetCore.Builder;
using src.Middleware;

namespace Microsoft.AspNetCore.Builder
{
    public static class ApiLoggingHandlerExtensions
    {

        public static IApplicationBuilder UseWebAPILogger(this IApplicationBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentException(nameof(app));
            }
            return app.UseMiddleware<CustomLoggingMiddleware>();
        }
    }
}